# Java基础

### 值传递和引用传递

1、基本类型和引用类型

```
a、常见的基本数据类型：byte,short,int,long,char,float,double,Boolean,returnAddress
	这个是按值调用
b、引用类型：引用类型变量保存引用值，引用值就是对象所在内存空间的"首地址值"，通过引用值来操作对象
   常见的引用类型：类类型、接口类型和数组
    这个是按引用调用
```

2、值传递：

```
a、在方法调用过程中，实参把实际参数传递给形参，此传递过程是将实参的值复制一份到函数中，在函数中对该值进行了操作，不会影响实参的值。因为是直接复制，这种方式传递大量数据时，运行效率会比较低下
b、如String类，设计师不可变的，so，每次赋值都是重新创建一个新的对象。so是值传递
```

3、引用传递

```
a、弥补了值传递的不足，如果传递的数据大，直接复制过去，内存会大量占用
b、引用传递就是将对象的地址值传递过去，函数接收的是原始值的首地址值
c、在方法执行中，形参和实参的内容相同，指向的是同一块内存区域
```

总结：

```
a、基本数据类型传值，对形参的修改不会影响实参
b、引用类型引用，形参和实参指向同一个内存地址，so，参数的修改会影响到实际的对象
c、String ,Interger等类型特殊，可以理解为传值，不会改变实参的对象
```

### String、StringBuffer、StringBuilder

1、String 创建机制

```
a、创建一个字符串时，首先检查池中是否有值相同的字符串对象，如有不需要创建直接从池中查找到对象引用。如没有则创建新的字符串对象，返回对象的引用，并放入池中。
b、String是final类型，它不可变，拼接、裁剪等，都会产生新的String对象
```

2、.StringBuffer/StringBuilder

```
a、底层存储方式都是通过char类型数组和String一样
b、和string的不同的是，值改变的时候，对象引用不会发生改变。数组会进行扩容。
c、StringBuffer是加锁的 StringBuilder没有
```

3、String 不可变的好处

```
a、线程安全
b、可以缓存hash值
```

### 面向对象基础

1、面向对象思想

```
定义：封装时根据不同的功能，进行不同的封装，功能类似的封装在一起。结构比较清晰
特点：a、复杂事情简单化 b、将我们从执行者边城了指挥者
```

2、对象创建的过程

```
A s = new A();
a、加载A.class文件进内存
b、在栈内存中为s开辟空间
c、在堆内存中为对象开辟空间
d、对A对象的成员变量进行默认初始化
e、对A对象的成员变量进行显示初始化
f、通过构造对成员变量赋值
g、对象初始化完毕，把对象地址赋值给s变量
```

3、成员变量和局部变量区别

```
a、在类中位置不同。成员变量在类中方法外；局部变量在方法定义中或者方法声明上
b、在内存中的位置不同。成员变量在堆内存；局部变量在栈内存
c、生命周期不同。成员变量随着对象的创建而存在，随着对象的消失消失；局部变量：随着方法的调用而存在，随着方法的调用完毕二结束
d、初始化不同：成员变量有默认值；局部变量：必须定义，赋值，然后才可使用。
   局部变量名称和成员变量名称一样，在方法中使用的时候，使用就近原则。
```

4、面向对象的特征：封装、继承、多态

5、封装

```
定义：隐藏对象的属性和实现细节，仅对外提供公共访问方式
好处：a、隐藏实现细节，提供公共访问方式
	 b、提高代码复用性
原则：将不需要对外提供的内容隐藏起来
```

6、继承

```
定义：多个类存在相同的属性和行为时，将这些内容抽取到单独一个类中，多个类无需在定义这些属性和行为。只需要继承
好处：a、提高了代码复用性
	 b、提高了代码的维护性
	 c、让类与类产生关系，多态的前提
弊端：a、类的耦合增加了
	 b、开发原则：高内聚、低耦合
	 耦合：类与类的关系  内聚：自己完成某件事情的能力
```

7、多态（多个运行状态）

```
定义：程序中定义的引用变量所指向的具体类型和通过该变量发出的方法调用在编程时并不确定。
它是在程序运行期间确定具体是哪个类;so，可以让引用变量绑定到各种不同的类的实现上，从而导致该引用调用的具体方法随着改变，即不修改程序代码就可以改变程序运行时所绑定的具体代码，让程序可以选择多个运行状态。

条件：继承、重写、向上转型
继承：在多态中必须存在有继承关系的子类和父类。
重写：子类对父类中某些方法进行重新定义，在调用这些方法时就会调用子类的方法。
向上转型：在多态中需要将子类的引用赋给父类对象，只有这样该引用才能够具备技能调用父类的方法和子类的方法
```

8、接口

```
定义：接口是一种抽象类型，是服务提供者和使用者之间的一个协议。
     jdk1.8前是抽象方法的集合，一个类通过实现接口从而来实现两者间的协议。
     接口可定义字段和方法。在jdk1.8前，接口中所有方法都是抽象的，1.8之后，可以在接口中编写默认方法和静态方法。
     
jdk1.8之后例子：
public interface E {
    String NAME = "gyf";//默认修饰类型：public static final
    //静态方法 可以省略public声明
    static void method(){
        System.out.println("rxy");
    }
    //默认方法
    default void method2(){
        System.out.println("E method2");
    }
    
特点：
1、jdk1.8 之前如果给接口增加一个方法，需要修改所有实现该接口的类（维护成本比较高）。
   jdk1.8之后可以通过给接口增加默认方法实现，达到这个需求。
2、接口的特点：
	a、接口没有构造方法
	b、接口不能用于实例化对象
	c、接口中的字段必须初始化，并隐示的设置为public static final 。因为为了符合规范，接口中的字段名要全部大写。
	d、接口是被实现的，不是被继承的
	e、接口中每个方法默认是public abstract修饰的（隐示修饰），jdk1.8之后，在接口中可以编写静态的方法和默认的方法。声明默认方法需要使用关键字default.(不允许定义private或者protected修饰)
	f、类实现接口的时候，必须实现所以的方法，否则类必须声明为抽象的
	g、接口支持多重继承。可以继承多个接口
	
Marker Interface(空接口)：有些接口不仅仅限于抽象方法的集合，其实有各种不同的实践。有一个没有任何方法的接口。常见的比如Serializable.

java8的变化：主要是增加了对default method的支持。还有lambda|Stream相关功能。
```

9、抽象类

```
定义：所有的对象都是通过类来进行描绘的。但并不是所有类都是用来描绘对象的，如果一个类中没有包含足够的信息来描绘一个具体的对象，这样的类就是抽象类。

特点：
a、抽象类除了不能实例化对象，类的其他功能都一样。
b、抽象方法一般在抽象类中。主要是为了代码的重构
c、抽象类大多用于抽取相关java类的公共的方法和公共的成员变量，然后通过继承的方式达到代码的复用目的
```

10、接口和抽象类区别

```
语法区别
1、接口中定义的变量，默认是static final类型的需要初始化。
   抽象类中可以正常定义自己的数据成员，不需要进行初始化。
2、接口中一般方法都是abstract.jdk1.8之后可以定义默认的default method
编程角度
1、抽象类是一种继承关系，一个类只能一次继承关系。但是一个类却可以实现多个接口。
2、抽象类方法中可以定义默认的行为，接口一般不可以，不过在jdk1.8之后，可以定义default method,实现默认方法。jdk1.8之前，接口和实现类之间的耦合度比较高。当需要接口中加一个方法的时候，所有的实现类都必须随之改变。

换种说法：接口是动作的抽象（这个东西能做什么），抽象类是根源的抽象（这个对象是什么）。

使用哪一种的场景:
使用接口的场景
	1、需要让不相关的类实现一个方法
	2、需要使用多重继承
使用抽象类的场景：
	1、需要在几个相关的类中共享代码
	2、需要能控制继承来的成员的访问权限，而不是都为 public
	3、需要继承非静态和非常量字段
```

### 内部类

```
分类:
1、静态内部类
2、成员内部类
3、局部内部类
4、匿名内部类

===========
1、定义：把类定义在类的内部，称为是内部类
2、内部类访问特点：a、内部类可以直接访问外部类的成员（包括私有）b、外部类访问内部类成员需要创建对象
3、作用：功能的隐藏，减少内存开销，提高效率

成员内部类：
位置划分：成员内部类、局部内部类。
成员内部类访问：外部类名.内部类名 对象名 = 外部类对象.内部类对象;
a、private 为了保证数据的安全性
b、static 为了方便访问数据
注意:
1、静态内部类访问的外部类数据必须用静态修饰。
2、成员方法可以是静态的也可以是非静态的
===========
内部类和外部类联系：内部类可以访问外部类所有的方法和属性，如果内部类和外部类有相同的成员方法和成员属性，内部类的成员方法调用要优先于外部类即内部类的优先级比较高（外部类只能访问内部类的静态常量或者通过创建内部类来访问内部类的成员属性和方法。）


成员内部类
public class Outer {

    private int value = 10;
    
    private static int staticValue=11;
    
    protected  class Nested {
    	public int getValue() {
    		// 可以访问外部类的非静态、静态、私有成员
    		return value+staticValue;
    	}
    }
}
public static void main(String[] args) {
	Outer outer=new Outer();
	Outer.Nested nested=outer.new Nested();
	System.out.println(nested.getValue());
}

布局内部类
public interface Logger {
	void log(String message);
}

public class Outer {

	String time = LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));

	public Logger getLogger() {
		class LoggerImpl implements Logger {
			@Override
			public void log(String message) {
				System.out.println(time + " : " + message);
			}
		}
		return new LoggerImpl();
	}
}

public static void main(String[] args) {
	Outer outer=new Outer();
	Logger logger=outer.getLogger();
	logger.log("Hi");
}
```

### 方法重写和方法重载

1、方法重写

```
定义：子类中出现了和父类中一模一样的方法声明(方法名,参数列表,返回值类型)，也叫方法覆盖。
应用：子类需要父类的功能，而又要有自己特有的内容。
注意：a、父类私有方法不能被重写 b、子类重写后访问权限不能设置低，尽量保持一致
作用：一般是覆盖公共的和受保护的方法。

重写只能用于实例方法，不能用于静态方法。
```

2、方法重载

```
定义：一个类中，有若干个方法名字相同，但方法参数形式不同
原则：受影响：1、方法名字相同 2、参数类型不同 3、参数个数不同；
    不受影响：1、方法的参数名 2、返回值 3、修饰符号
```

### Serializable

1、实现这个接口目的：主要是为了序列化

2、标识类：空接口，没什么具体内容，目的是为了简单标识一个类对象是否可以被序列化

3、序列化的场景：

```
a、内存中的对象写入磁盘
b、socket网络中传输对象
c、远程传输
```

4、通过判断serialVersionUID来验证版本一致性来进行反序列化。

5、serialVersionUID生成的两种方式：a、默认1L b、根据类名、接口名、方法名等生成64位的hash字段