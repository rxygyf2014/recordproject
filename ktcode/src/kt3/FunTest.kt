package kt3


fun main() {
//    val blessingFunction: () -> String = {
//        val holiday = "New Year."
//        "Happy $holiday"
//    }
//    val blessingFunction: (String) -> String = { name->
//        val holiday = "New Year."
//        "$name Happy $holiday"
//    }
//    val blessingFunction: (String) -> String = {
//        val holiday = "New Year."
//        "$it Happy $holiday"
//    }
//    val blessingFunction = {
//        val holiday = "New Year."
//        " Happy $holiday"
//    }

//    val blessingFunction: (String, Int) -> String = { name, year ->
//        val holiday = "New Year."
//        "$name Happy $holiday$year"
//    }
    val blessingFunction = { name:String, year:Int ->
        val holiday = "New Year."
        "$name Happy $holiday$year"
    }
    println(blessingFunction("ryq",20))

}
