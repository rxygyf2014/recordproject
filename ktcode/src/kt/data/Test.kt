package kt.data

fun main() {
//    dataTest()
//    letTest()
//       withTest()
//      runTest()
//        applyTest()
    alsoTest()
}

//also 和let有点像，但是返回的是对象本身
fun alsoTest() {
    val also = "ryq".also {
        println(it.length)
    }
    println(also)
}

//返回对象本身和run性质一样
fun applyTest() {
    val data2 = DataSource.Data2(12, "rxy")
    val result = data2.apply {
        name = "gyf"
        println("my name is $name")
        28
    }
    println("result:$result")
}

//返回最后一行return + 传递一个lambad表达
fun runTest() {
    val data2 = DataSource.Data2(12, "rxy")
    val result = data2.run {
        println("my name is $name")
        28
    }
    //返回28
    println("result:$result")

}

//with 返回传入的对象
fun withTest() {
    val data2 = DataSource.Data2(12, "rxy")
    with(data2){
        println(name)
        name ="ryq"
    }
    println(data2)
}

//data的class测试
fun dataTest() {
    val data2 = DataSource.Data2(12, "rxy")
    println("info===$data2")
}

//let 返回最后一行的命令
fun letTest() {
    val result = "rxy".let {
        println(it.length)
        1000
    }
    println(result)
}
