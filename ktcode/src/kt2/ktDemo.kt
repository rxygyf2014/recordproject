package kt2

import com.lstech.sdk.demo.fragment.LocalBleInfo
import com.lstech.sdk.demo.fragment.LocalBleInfo.currentBle
import kt2.bean.UserBean
import sun.rmi.runtime.Log


fun main() {
//    test1()
    test2(5)
//      test3()
//    test4()
//    test5()
//    test6()
//    test7()
}

var searchName = { val s = "Q20";"Lnv" }
fun test7() {

//    bar1("1","2")
//    bar1("Q20","Lnv")
    LocalBleInfo.cacheMap[currentBle]?.let {
        println("开启鉴权")
    }
//    println( LocalBleInfo.cacheMap)
}

fun bar1(vararg args: String) {
    println(args.contentToString())
}

fun test6() {
    if (LocalBleInfo.mac == "") {
        println("空")
    } else {
        println("有数据")
    }

}

fun test5() {
    val list = mutableListOf(29, 20, 19, 10, 8, 6, 2)
    val insertList = mutableListOf<Int>() as ArrayList
//    println(list.subList(3,list.size))
    println(list.subList(0, 3))
    val subList = list.subList(0, 3)
    val all = list.removeAll(subList)
    println(list)
}

fun test4() {
    val hashMap: HashMap<String, String> = HashMap<String, String>()
    hashMap["name"] = "gyf"
    hashMap["age"] = "18"

    println("name${hashMap["name"]}")
}

fun test3() {
    val list = mutableListOf(29, 20, 19, 10, 8, 6, 2)
    val list2 = mutableListOf(9, 6, 2)
    val insertList = mutableListOf<Int>()
    list.forEach {
        if (!list2.contains(it)) {
            insertList.add(it)
        }
    }
    println(insertList)
}

fun test2(currentRate: Int) {
    val list: ArrayList<Int> = ArrayList()
    list.add(30)
    list.add(20)
    list.add(10)
//    val list = mutableListOf(29, 20, 19, 10, 8, 6, 2)
//    val first = list.last {
//        it > currentRate
//    }
//    println(first)
//    list.forEachIndexed { index, it ->
//        if (tempMax > it) {
//            println("rxy==$index")
//            mIndex = index
//            return
//        }
//    }

    println("=" + getCurrentRanking(list, currentRate))
}

/**
 * 返回当前排行 :index+1
 */
private fun getCurrentRanking(list: List<Int>, currentRate: Int): Int {
    var tempIndex = 1
    list.forEach {
        if (currentRate < it) {
            tempIndex++
        }
    }
    return tempIndex
}

fun test1() {
    var name: String? = null
    name = "rxy"!!
    println("name$name")
    //这种需要校验为空
//    var d:String =null!!
    var d: String? = null
    var b: Int = 123
    val c = d ?: b
    println(c)

    val myList: ArrayList<String>? = null
    println("-->> List Size = " + myList?.size)
//    println( "-->> List Size = " + myList!!.size)
}
