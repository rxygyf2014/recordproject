package kt2

import kt2.bean.UserBean

//https://blog.csdn.net/cunchi4221/article/details/107478985
fun main() {
//    lambda1()
    lambdaHigh()
}

//高阶函数
fun lambdaHigh() {
//    high1()
//    high2()
//    high3()
    high4()
}

val user by lazy { UserBean("rxy",18) }
fun high4() {
    println(user.name)
}

//将lambda作为高级的参数传递
//lambda表达式作为参数在另一个函数中作为函数运行
fun high3() {
    fun printMe(str:(String)->Unit){
        str("rxy")
    }
    printMe { println(it.length) }
    fun printMe2(str:()->Unit){
        str()
    }
    printMe2 { println("rxy") }


}

//将函数作为参数传递给另一个函数,使用::
fun high2() {
    fun printFun(str:String){
        println("输入$str")
    }
    fun funExample(str:String,expression:(String)->Unit){
        expression(str)
    }

    //具体调用
    funExample("rxy",::printFun)


}

fun high1() {
    var printFun :(String)->Unit={
        println("输入的 $it")
    }

    fun funExample(str:String,expression:(String)->Unit){
        expression(str)
    }
    //调用高阶函数
    funExample("rxy",printFun)
}

fun lambda1() {
    //    lambdaFun()
//    lambdaFun2()
//    lambdaFun3()
}

//不传参数+无返回
fun lambdaFun3() {
    val  result :()->Unit ={
        println("rxy")
    }
//    var result ={
//        println("rxy")
//    }
    println(result())
}

//传递参数 +有返回的写法
fun lambdaFun2() {
    val result: (String) -> String = { s: String ->
        s+"rxy"
    }
    println(result("gyf"))
}

//lambda 函数  传递参数 +无返回的写法
fun lambdaFun() {
    var lambdaFunction: (String) -> Unit = { s: String -> println(s) }
    lambdaFunction("rxy")
    //or
    lambdaFunction = { println(it) }
    lambdaFunction("gyf")
}
