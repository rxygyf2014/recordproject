package kt2;

public class BleDevice {
    public static void main(String[] args) {
        secToTime(64);
    }

    public static void secToTime(int time) {
        int  minute = time / 60;
        int second = time % 60;
        System.out.println(unitFormat(minute)+"‘"+unitFormat(second)+"''");
    }

    private static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + i;
        else
            retStr = "" + i;
        return retStr;
    }
}
