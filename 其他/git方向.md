# git方向记录

## 1、git忽略文件不生效问题

- 参考:

  ```
  https://www.jianshu.com/p/3c56e2e5e4dc?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation
  ```

- 主要命令：

  ```
  git rm -r --cached .
  git add .
  git commit -m "clear cached"
  ```


