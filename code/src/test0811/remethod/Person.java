package test0811.remethod;

/**
 * @ author rxy
 * @ date 2021/8/12 17:06
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT father
 */

/**
 * 方法重写，子类完全和父类的方法名字和参数一样
 */
public abstract class Person {
    abstract void run();
    public void eat(String food){
        System.out.println("person eat"+food);
    }
    protected void method(){
        System.out.println("person method");
    }
    void learn(){
        System.out.println("person learn");
    }
}
