package test0811.remethod;

/**
 * @ author rxy
 * @ date 2021/8/12 17:07
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT child
 */
public class Man extends Person{
    @Override
    void run() {
        System.out.println("child run");
    }

    @Override
    public void eat(String food) {
        super.eat(food);
        System.out.println("child eat"+food);
    }

    @Override
    protected void method() {
        super.method();
        System.out.println("child method");
    }

    @Override
    void learn() {
        super.learn();
        System.out.println("child learn");
    }
}
