package test0811.remethod;

/**
 * @ author rxy
 * @ date 2021/8/12 17:05
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT 重写和重载
 */
public class ReMethod {
    public static void main(String[] args) {
        Man man = new Man();
        man.eat("milk");
        man.method();
    }

    //下面是重载:1、方法名字相同 2、参数类型不同 3、参数个数不同
    //不受影响：1、方法的参数名 2、返回值 3、修饰符号
    public void method(){

    }
    public void method(String name){

    }
    public void method(int age){

    }
}
