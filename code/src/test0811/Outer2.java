package test0811;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * @ author rxy
 * @ date 2021/8/12 16:37
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT 局部内部类
 */
public class Outer2 {
    String time = LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));

    public Logger getLogger() {
        class LoggerImpl implements Logger {
            @Override
            public void log(String message) {
                System.out.println(time + " : " + message);
            }
        }
        return new LoggerImpl();
    }

    public static void main(String[] args) {
        Outer2 outer=new Outer2();
        Logger logger=outer.getLogger();
        logger.log("Hi");
    }

    interface Logger {
        void log(String message);
    }

}




