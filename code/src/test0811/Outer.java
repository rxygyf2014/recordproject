package test0811;

/**
 * @ author rxy
 * @ date 2021/8/11 21:22
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT 成员内部类
 */
public class Outer {
    private int value = 10;

    private static int staticValue = 11;

    protected class Nested {
        public int getValue() {
            return value + staticValue;
        }
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        Nested nested = outer.new Nested();
        System.out.println(nested.getValue());
    }
}
