package test0818;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author rxy
 * date 2021/8/18 20:45
 * TEL 18610643633
 * EMAIL rxygyf2014@163.com
 * CONTENT 生产者消费者模式 Lock中Condition的await/signalAll
 */
public class ProductAndConsumer {
    private static ReentrantLock lock = new ReentrantLock();
    private static Condition full = lock.newCondition();
    private static Condition empty = lock.newCondition();

    public static void main(String[] args) {
        List<Integer> linkedList = new ArrayList<>();
        ExecutorService service = Executors.newFixedThreadPool(15);
        for (int i = 0; i < 5; i++) {
            service.execute(new Product(linkedList, 8, lock));
//            service.submit(new Product(linkedList, 8, lock));
        }
        for (int i = 0; i < 10; i++) {
            service.execute(new Consumer(linkedList, lock));
//            service.submit(new Consumer(linkedList, lock));
        }
    }


    static class Consumer implements Runnable {
        private Lock lock;
        private List<Integer> list;

        public Consumer(List<Integer> list, Lock lock) {
            this.lock = lock;
            this.list = list;
        }

        @Override
        public void run() {
            while (true) {
                lock.lock();
                try {
                    while (list.isEmpty()) {
                        empty.await();
                    }
                    Integer remove = list.remove(0);
                    System.out.println("消费数据" + remove);
                    full.signalAll();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    static class Product implements Runnable {
        private List<Integer> list;
        private int maxLength;
        private Lock lock;

        public Product(List<Integer> list, int maxLength, Lock lock) {
            this.list = list;
            this.maxLength = maxLength;
            this.lock = lock;
        }

        @Override
        public void run() {
            while (true) {
                lock.lock();
                try {
                    while (list.size() == maxLength) {
                        full.await();
                    }
                    Random random = new Random();
                    int i = random.nextInt();
                    list.add(i);
                    System.out.println("生产者" + i);
                    empty.signalAll();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }
    }
}
