package test0818;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author rxy
 * date 2021/8/18 20:45
 * TEL 18610643633
 * EMAIL rxygyf2014@163.com
 * CONTENT 生产者消费者模式 :LinkedBlockingDeque
 */
public class ProductAndConsumer2 {
    private static LinkedBlockingDeque<Integer> deque = new LinkedBlockingDeque<>(8);

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(15);
        for (int i = 0; i < 5; i++) {
            service.submit(new Product(deque));
        }
        for (int i = 0; i < 10; i++) {
            service.submit(new Consumer(deque));
        }
    }


    static class Consumer implements Runnable {
        LinkedBlockingDeque<Integer> deque;

        public Consumer(LinkedBlockingDeque<Integer> deque) {
            this.deque = deque;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    Random random = new Random();
                    int i = random.nextInt();
                    deque.put(i);
                    System.out.println("生产"+i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Product implements Runnable {
        LinkedBlockingDeque<Integer> deque;

        public Product(LinkedBlockingDeque<Integer> deque) {
            this.deque = deque;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    Integer integer = deque.take();
                    System.out.println("消费进程=="+integer);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
