package test0818;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author rxy
 * date 2021/8/18 20:45
 * TEL 18610643633
 * EMAIL rxygyf2014@163.com
 * CONTENT 生产者消费者模式 wait/notify
 */
public class ProductAndConsumer3 {


    public static void main(String[] args) {
        List<Integer> linkedList = new ArrayList<>();
        ExecutorService service = Executors.newFixedThreadPool(15);
        for (int i = 0; i < 5; i++) {
            service.execute(new Product(linkedList, 8));
//            service.submit(new Product(linkedList, 8, lock));
        }
        for (int i = 0; i < 10; i++) {
            service.execute(new Consumer(linkedList));
//            service.submit(new Consumer(linkedList, lock));
        }
    }


    static class Consumer implements Runnable {
        private List<Integer> list;

        public Consumer(List<Integer> list) {
            this.list = list;
        }

        @Override
        public void run() {
            try {
                //1、循环调用
                while(true){
                    //2、加锁
                    synchronized (list){
                        //3、休眠的条件
                        while (list.isEmpty()){
                            list.wait();
                        }
                        //4、消耗数据
                        Integer remove = list.remove(0);
                        System.out.println("消耗的数据"+remove);
                        list.notifyAll();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    static class Product implements Runnable {
        private List<Integer> list;
        private int maxLength;

        public Product(List<Integer> list, int maxLength) {
            this.list = list;
            this.maxLength = maxLength;
        }

        @Override
        public void run() {
            try{
                while (true) {
                    synchronized (list){
                        while(list.size()==maxLength){
                            list.wait();
                        }
                        Random r=new Random();
                        int i = r.nextInt();
                        System.out.println("生产的数据"+i);
                        list.add(i);
                        list.notifyAll();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}
