package test0812;

import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

/**
 * @ author rxy
 * @ date 2021/8/13 11:01
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT lambda
 */
public class LambdaTest {
    public static void main(String[] args) {
        List<Student> studentList = getDataList();
        List<Student> startsWithYeList = filterStudent(studentList, student -> student.getName().startsWith("小杨"));
        //拆分写法
        Filter<Student> filter = student -> student.getAge() > 20;//lambda表达式复制给接口
        List<Student> ageList = filterStudent(studentList, filter);
        System.out.println(ageList);
        filterStudent(studentList, LambdaTest::test);

//        List<String> list = new ArrayList<>();
//        list.stream()
//                .filter(s -> s.length()>3)
//                .mapToInt(value -> 0)
//                .sum();
        final Clock clock = Clock.systemUTC();
        System.out.println( clock.instant() );
        System.out.println( clock.millis() );

//        HashMap<String,String>
        HashSet<String> hashSet=null;
        testBase64();
    }

    private static void testBase64() {
        final String text = "Base64 finally in Java 8!";

        final String encoded = Base64
                .getEncoder()
                .encodeToString( text.getBytes( StandardCharsets.UTF_8 ) );
        System.out.println( encoded );

        final String decoded = new String(
                Base64.getDecoder().decode( encoded ),
                StandardCharsets.UTF_8 );
        System.out.println( decoded );
    }

    //静态方法的方法引用
    private static boolean test(Student student) {
        return student.getAge() > 20;
    }
    private static List<Student> filterStudent(List<Student> studentList, Filter<Student> filter) {
        List<Student> students = new ArrayList<>();
        for (Student student : studentList) {
            if (filter.filter(student)) {
                students.add(student);
            }
        }
        return students;
    }

    public interface Filter<T>{
        boolean filter(T t);
    }
//    public interface Filter{
//        boolean filter(Student student);
//    }
    private static List<Student> getDataList() {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("小杨逗比", 24));
        studentList.add(new Student("潇湘剑雨", 25));
        studentList.add(new Student("你是傻逼", 18));
        return studentList;
    }
}
