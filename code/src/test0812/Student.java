package test0812;

/**
 * @ author rxy
 * @ date 2021/8/12 21:18
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT data
 */
public class Student {
    private String name="rxy";
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public void method(){
        System.out.println("=="+name);
    }
}
