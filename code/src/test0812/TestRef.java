package test0812;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * @ author rxy
 * @ date 2021/8/12 21:01
 * @ TEL 18610643633
 * @ EMAIL rxygyf2014@163.com
 * @ CONTENT ref引用的定义
 */
public class TestRef {
    public static void main(String[] args) {
        Student data = new Student("rxy",12);
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        System.out.println(referenceQueue.poll());
        WeakReference<Student> softReference = new WeakReference<>(data,referenceQueue);
        Student refData = softReference.get();
        assert refData != null;
        refData.method();
    }
}
