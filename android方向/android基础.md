## 动态添加xml布局文件
    java代码控制
    1、xml写好，然后通过LayoutInflater生成一个View,然后add到布局容器中；
    2、用法：LayoutInflater.from(this).inflate(布局, 将要添加的父容器, false)；
    3、解释：false表示没有把布局文件添加到父布局中，true表示把布局文件添加到容器中。false的话就是还需要add
        //清空布局
        fl_container.removeAllViews();
        //删除子布局
        fl_container.removeView(inflateView);
## 布局测量长宽

```
1、通过ViewTreeObserver监听;获取长宽后为了避免频繁调用触发，需要进行解注册
ViewTreeObserver observer = view.getViewTreeObserver();    
observer .addOnGlobalLayoutListener(new OnGlobalLayoutListener() {    
           @Override    
           public void onGlobalLayout() {    
              view.getViewTreeObserver().removeGlobalOnLayoutListener(this);    
              final int w = view.getMeasuredWidth();  
              final int h = view.getMeasuredHeight();  
           }    
       });   
       
2、通过handler
   view.post(new Runnable() {
       @Override
       public void run() {
       //获取长宽
       }
	});    
    
3、通过onWindowFocusChanged
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus) {
            int width = view.getMeasuredWidth();
            int height = view.getMeasuredHeight();
        }
    }
```

